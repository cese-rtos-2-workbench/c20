/*
 * Copyright (c) 2023 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : app.c
 * @date   : Feb 17, 2023
 * @author : Sebastian Bedin <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "main.h"
#include "cmsis_os.h"

/********************** macros and definitions *******************************/

#define TX_EXAMPLE_     (1)
#define RX_EXAMPLE_     (2)
#define EXAMPLE_        RX_EXAMPLE_

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static char mgs1[] = "Hola";
static char mgs2[] = "Chau";
static bool tx_complete;

static uint8_t rx_buffer[128];
static uint8_t rx_out[128];
static bool rx_reset;
static bool rx_complete;
static uint16_t rx_size;
static uint16_t rx_size_acc;

/********************** external data definition *****************************/

extern UART_HandleTypeDef huart3;

/********************** internal functions definition ************************/

static void task_tx_example_(void* argument)
{
  while(true)
  {
    HAL_UART_Transmit_IT(&huart3, mgs1, strlen(mgs1));

    vTaskDelay((TickType_t)((1000) / portTICK_PERIOD_MS));
  }
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t size)
{
  UNUSED(huart);
  rx_complete = true;
  rx_size = size;
  rx_buffer[size] = '\0';
}

static void task_rx_example_(void* argument)
{
  rx_size_acc = 0;
  rx_complete = false;
  HAL_UARTEx_ReceiveToIdle_IT(&huart3, rx_buffer, 128);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET);
  while(true)
  {
    if(rx_complete)
    {
      rx_complete = false;
      if(0 < rx_size)
      {
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET);
        snprintf(rx_out, 128, "%s\r\n", rx_buffer);
        HAL_UART_Transmit_IT(&huart3, rx_out, strlen(rx_out));
      }
      HAL_UARTEx_ReceiveToIdle_IT(&huart3, rx_buffer, 128);
    }
    else
    {
      HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET);
    }

    vTaskDelay((TickType_t)((100) / portTICK_PERIOD_MS));
  }
}

/********************** external functions definition ************************/

void app_init(void)
{
  // tasks
  {
    BaseType_t status;

#if TX_EXAMPLE_ == EXAMPLE_
    status = xTaskCreate(task_tx_example_, "task_tx_example", 128, NULL, tskIDLE_PRIORITY, NULL);
    while (pdPASS != status)
    {
      // error
    }
#endif

#if RX_EXAMPLE_ == EXAMPLE_
    status = xTaskCreate(task_rx_example_, "task_rx_example", 128, NULL, tskIDLE_PRIORITY, NULL);
    while (pdPASS != status)
    {
      // error
    }
#endif
  }
}

/********************** end of file ******************************************/
